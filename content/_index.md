+++
title = "CDP Dojo"
chapter = true
weight = 5
+++

# T-Mobile CDP Dojo

The T-Mobile CDP Dojo reinforces modern development practices. Teams attending the Dojo will leave their normal
workspace and come down to the Dojo for a one-week period to accomplish two goals:

1. Facilitate and empower
development teams
Learning is a core tenet of the T-Mobile culture, and is necessary to keep up with the ever-changing world of
technology. Teams must be willing to commit to learning and accept that they may need to temporarily slow their current
deliverables to facilitate the educational activities.

2. Deliver real work During the initial educational phase of the Dojo, curated exercises will demonstrate
the capabilities of CDP. Following that, the two-day capstone project will leverage those skills to migrate
applications to CDP and begin delivering value to customers. All of this is performed with direct guidance and
assistance from the Dojo senseis.
