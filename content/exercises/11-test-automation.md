+++
title = "11. Test Automation"
date = 2019-12-18T10:58:34-05:00
weight = 211
chapter = true
+++
##### Exercise
# Test Automation

#### Outcomes

By the end of this exercise:

1. You will create a test stage in your pipeline
2. You will run an automated test in Gitlab CI
3. You will create a tests for the controllers and test it in an automated fashion


#### Prerequisites

This exercise assumes that you meet the following Prerequisites

1. You should have the sample application cloned in your docker environment 
2. You should know how to run the docker exercise container
3. You should be familiar with units tests

---

1. Copy over the Controller Test files, the HelloWorldControllerTest, StudentControllerIT, and StudentControllerTest.Java 
to src/test/java/com/tmobile/cdp/contino/dojosampleapp/controller/

{{%attachments title="Java Tests file" style="grey" pattern=".*java"/%}}

In a new terminal window get the name of the running Dojo Docker container
```bash 
○ → docker ps
CONTAINER ID        IMAGE                                                     COMMAND             CREATED             STATUS              PORTS               NAMES
9cf6ac2e310d        registry.gitlab.com/tmobile/cdp/contino/dojo-docker:prd   "/bin/bash"         19 minutes ago      Up 19 minutes                           ecstatic_kepler
```

Then use docker to copy in the files. Use the docker container name that you see for $DOCKER_NAME. Above it is ecstatic_kepler, but yours will be different. 

```bash
docker cp HelloWorldControllerTest.java $DOCKER_NAME:/root/dojo-sample-app/src/test/java/com/tmobile/cdp/contino/dojosampleapp/controller/
```

Repeat for each Test Java file.

2. Copy the Maven project templates from Test-Automation.yml to your .gitlab-ci.yml
   
{{%attachments title="Gitlab CI test automation file" style="grey" pattern="Test-Automation.yml"/%}}
   
3. Verify the stages: *build, test, publish* , underneath tmo, to the pipeline in the .gitlab-ci.yml file.

    ```yaml
    stages:
         - tmo
         - build
         - test
         - publish
         - package
    ```

4. Commit the changes to your branch 

```bash
git status
git add -A
git commit -m "added Controller tests"
git push origin $LOCAL_BRANCH
```

5. Check the pipeline to see if your changes have run. 

Test Automation!
