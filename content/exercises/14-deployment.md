+++
title = "14. Deployments"
date = 2020-01-16T14:18:23-05:00
weight = 214
chapter = true
+++

##### Exercise
# Deployment

#### Outcomes

By the end of this exercise:

1. You will successfully deploy a Spring-Boot application to a Kubernetes cluster
1. You will add a deployment job into our pipeline


#### Prerequisites

This exercise assumes that you meet the following Prerequisites

1. You should have completed exercises 7 and 8
1. You should know how the `Dockerfile' and how an image is built using it.
1. You should be relatively familiar with YAML syntax.

---


{{% notice warning %}}
Make sure the dojo-sample-app is on branch exercise-14
{{% /notice %}}

```bash
git checkout exercise-14
```

Then checkout your branch

```bash
git checkout -b exercise-14-YOURNAME
```

### 1. Start with our .gitlab-ci.yml updates.
{{%expand%}}
First, we want to import the Helm deploy job from T-Mobiles templates. We can do this by adding the following
lines after the `includes:` line in our .gitlab-ci.yml file.
Make sure that this is indented and aligned with the other import statements.

```Yaml
# Injects helm deploy function
- project: "tmobile/templates"
  ref: tmo/master
  file: "/gitlab-ci/.tmo.function.helm-deploy.gitlab-ci.yml"
```
{{%/expand%}}
### 2. Update Helm App Name
{{%expand%}}
Change the name of the application deployment be editing the variable HELM_APP_NAME

```yaml
variables:
   HELM_APP_NAME: dojo-sample-app-YOUR-BRANCH-NAME
````
{{%/expand%}}
### 3. Update our stages
{{%expand%}}
We need to add a Deploy stage as the very last our stages definition inside our gitlab-ci.yml.
It should look something similar to this.

```yaml
stages:
  - tmo
  - build
  - test
  - publish
  - package
  - deploy
```
{{%/expand%}}
### 4. Define our Deployment job.
{{%expand%}}
We are going to add a new job to our pipeline. This job will only be deploying to the dev environment,
thus we will call it "deploy-dev".

Add the following lines to the end of the entire `.gitlab-ci.yml` file.

```yaml
# Deploy to Development environment
deploy-dev:
  extends: .helm_deploy
  environment:
    name: dev
  when: manual
```
If you want to see what the Helm Deploy template is doing you can see it here

https://gitlab.com/tmobile/templates/blob/tmo/master/gitlab-ci/.tmo.function.helm-deploy.gitlab-ci.yml


{{%notice info%}}
We do not need to declare the stage due to the fact that it is declared inside the T-Mobile template.
{{%/notice%}}
{{%/expand%}}
### 5. Confirm and Commit the changes to your gitlab-ci.yml file

{{%expand%}}
Example :
```yaml
include:
  # Injects TMO Global Shared (handles certs, lists blacklisted files etc.)
  - project: 'tmobile/templates'
    ref: tmo/master
    file: '/gitlab-ci/.tmo.global.common.gitlab-ci.yml'

  # Injects Maven Build
  - project: 'tmobile/templates'
    ref: tmo/master
    file: '/gitlab-ci/.tmo.job.mavenBuild.gitlab-ci.yml'

  # Injects Maven Publish
  - project: 'tmobile/templates'
    ref: tmo/master
    file: '/gitlab-ci/.tmo.job.mavenPublish.gitlab-ci.yml'

  - project: 'tmobile/templates'
    ref: tmo/master
    file: '/gitlab-ci/.tmo.job.docker.gitlab-ci.yml'
  # Injects helm deploy function
  - project: "tmobile/templates"
    ref: tmo/master
    file: "/gitlab-ci/.tmo.function.helm-deploy.gitlab-ci.yml"

stages:
  - tmo
  - build
  - test
  - publish
  - package
  - deploy

variables:
    # Common Parameters (used by build and deploy)
    APP_VERSION: "1.0"
    BUILD_NUMBER: "$CI_COMMIT_SHORT_SHA"
    DOCKER_BUILD_ARGS: "--build-arg JAR_FILE=${CI_PROJECT_NAME}-${APP_VERSION}-$CI_COMMIT_SHORT_SHA.jar"

    # Maven Parameters
    MAVEN_PUBLISH_ARGS: "-Dfile=target/${CI_PROJECT_NAME}-${APP_VERSION}-$CI_COMMIT_SHORT_SHA.jar"
    MAVEN_VERSION: "3.6.1-jdk-8"
    JACOCO_COVERAGE_REPORT: "yes"

    # Spring Parameters
    SPRING_PROFILES_ACTIVE: gitlab-ci

    # Helm Deploy Parameters
    KUBE_VERSION: "v1.15.0"
    HELM_VERSION: "v2.10.0"
    HELM_DEBUG: "true"
    HELM_DRY: "true"
    HELM_CHARTS_DIR: "$CI_PROJECT_DIR/charts/dojo-sample-app"
    HELM_VERSION: "v2.10.0"
    HELM_APP_NAME: dojo-sample-app-YOUR_NAME
    HELM_VALUES: "$CI_PROJECT_DIR/charts/dojo-sample-app/values.yaml"

# Deploy to Development environment
deploy-dev:
  extends: .helm_deploy
  environment:
    name: dev
  when: manual
```


Commit the changes to your gitlab-ci.yml file

```bash
git status
git add -A
git commit -m "Add kubernetes deployment files"
git push origin exercise-14-YOUR-NAME
```
{{%/expand%}}
### 6. Go the pipeline and deploy the app
{{%expand%}}
In the deploy-dev stage we set the when to manual. Go into gitlab ci console and deploy the application


![](/cdp/dojo/images/exercises/helm_deploy_pipeline.png)

1. Navigate to https://gitlab.com/tmobile/cdp/contino/dojo-sample-app/pipelines
2. Find your branch pipeline that is being built
3. Click on the status and see the build jobs running
4. If completed you can click Play

![](/cdp/dojo/images/exercises/helm_deploy_play.png)

When it finishes the job will show

![](/cdp/dojo/images/exercises/helm_deploy_complete.png)

An example build is [here](https://gitlab.com/tmobile/cdp/contino/dojo-sample-app/-/jobs/412239037)
{{%/expand%}}
