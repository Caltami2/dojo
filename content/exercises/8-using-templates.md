+++
title = "8. Using Templates"
date = 2019-12-18T10:58:34-05:00
weight = 208
chapter = true
+++
##### Exercise
# Using Templates

#### Outcomes

By the end of this exercise:

1. You will understand gitlab-ci templating and how it can be leveraged.
1. You will have migrated from using a self-maintained build template to importing t-mobile's templates wherever possible.
1. You will have developed an understanding of pipelines, stages, and jobs.


#### Prerequisites

This exercise assumes that you meet the following Prerequisites


1. You should have completed exercise 7 in this Dojo.
1. You should know about stages
1. You should be familiar with .gitlab-ci.yml

#### Quickstart instructions

{{% notice warning %}}
Continue working on your branch you created in Exercise 7
{{% /notice %}}

---


### 1. Review your .gitlab-ci.yml
{{%expand%}}
At this point you have gained familiarity with .gitlab-ci.yml and the power of pipelines, jobs, and stages.
Throughout this exercise we will take a look at and use a mature implementation of organized gitlab templating:
[T-Mobile's gitlab-ci templates](https://gitlab.com/tmobile/templates/tree/tmo/master)

Currently, your `.gitlab-ci.yml` looks like this:

```yaml
image: docker:latest
services:
  - docker:dind

stages:
  - build
  - package

variables:
  # Common Parameters (used by build and deploy)
  APP_VERSION: "1.0"
  BUILD_NUMBER: "$CI_COMMIT_SHORT_SHA"
  DOCKER_BUILD_ARGS: "--build-arg JAR_FILE=${CI_PROJECT_NAME}-${APP_VERSION}-$CI_COMMIT_SHORT_SHA.jar"

  # Maven Parameters
  MAVEN_VERSION: "3.6.1-jdk-8"
  # Spring Parameters
  SPRING_PROFILES_ACTIVE: gitlab-ci

maven-build:
  stage: build
  image: maven:${MAVEN_VERSION}
  script: "mvn package -B -Dfile=target/${CI_PROJECT_NAME}-${APP_VERSION}-$CI_COMMIT_SHORT_SHA.jar"
  artifacts:
    paths:
      - target/*.jar
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - mvn/

docker-package:
  stage: package
  script:
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  - docker build . -t "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA" $DOCKER_BUILD_ARGS
  - docker push  "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA"
```

<br/>
{{%/expand%}}
### 2 - Delete the image declaration and jobs in your .gitlab-ci.yml
{{%expand%}}
To proceed, we should delete the image deceleration at the beginning of our .gitlab-ci.yml file.

Delete the image lines.

```yaml
image: docker:latest
services:
  - docker:dind
```

AND also delete these lines.

```yaml
maven-build:
  stage: build
  image: maven:${MAVEN_VERSION}
  script: "mvn package -B -Dfile=target/${CI_PROJECT_NAME}-${APP_VERSION}-$CI_COMMIT_SHORT_SHA.jar"
  artifacts:
    paths:
      - target/*.jar
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - mvn/

docker-package:
  stage: package
  script:
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  - docker build . -t "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA" $DOCKER_BUILD_ARGS
  - docker push  "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA"

```
{{%/expand%}}
### 3 - Import the tmo template.
{{%expand%}}

We will import a template and declare a stage, and this will automatically add a job into our resulting pipelines.
This is the `tmo` stage, which will run prior to any jobs in our pipeline.

**Add these Import statements** to of your **.gitlab-ci.yml**.

This should be the first thing in the `.gitlab-ci.yml` file


```YAML
# Injection of templates
include:
  # Injects TMO Global Shared (handles certs, lists blacklisted files etc.)
  - project: 'tmobile/templates'
    ref: tmo/master
    file: '/gitlab-ci/.tmo.global.common.gitlab-ci.yml'

```

{{%/expand%}}
### 4 - Add the required tmo stages.
{{%expand%}}
Look for the `stages:` line of your `.gitlab-ci.yml`

Add the - tmo stage to be the **first stage** in your yaml file. Like so:

Then, add the - test stage after your 'build' stage.

Also add the - publish stage after your 'test' stage.
```YAML
stages:
  - tmo
  - build
  - test
  - publish
  - package
```
{{%/expand%}}
### 5 -  Now that you have added a new imported job, lets replace our current jobs with T-Mobile's templates.
{{%expand%}}
We are going to remove both of the jobs that we defined:
1. maven-build
1. docker-package

So after you do that your `.gitlab-ci.yml` should look like so :

```yaml
# Injection of templates
include:
  # Injects TMO Global Shared (handles certs, lists blacklisted files etc.)
  - project: 'tmobile/templates'
    ref: tmo/master
    file: '/gitlab-ci/.tmo.global.common.gitlab-ci.yml'

stages:
  - tmo
  - build
  - test
  - publish
  - package

variables:
    # Common Parameters (used by build and deploy)
    APP_VERSION: "1.0"
    BUILD_NUMBER: "$CI_COMMIT_SHORT_SHA"
    DOCKER_BUILD_ARGS: "--build-arg JAR_FILE=${CI_PROJECT_NAME}-${APP_VERSION}-$CI_COMMIT_SHORT_SHA.jar"

    # Maven Parameters
    MAVEN_PUBLISH_ARGS: "-Dfile=target/${CI_PROJECT_NAME}-${APP_VERSION}-$CI_COMMIT_SHORT_SHA.jar"
    MAVEN_VERSION: "3.6.1-jdk-8"

    # Spring Parameters
    SPRING_PROFILES_ACTIVE: gitlab-ci
```

{{%/expand%}}
### 6 - Lets add the remaining imports.
{{%expand%}}
Add the following imports after the first import we added earlier. Afterwards, your `.gitlab-ci.yml` should look like this:

```yaml
# Injection of templates
include:
  # Injects TMO Global Shared (handles certs, lists blacklisted files etc.)
  - project: 'tmobile/templates'
    ref: tmo/master
    file: '/gitlab-ci/.tmo.global.common.gitlab-ci.yml'

  # Injects Maven Build
  - project: 'tmobile/templates'
    ref: tmo/master
    file: '/gitlab-ci/.tmo.job.mavenBuild.gitlab-ci.yml'

  # Injects Maven Publish
  - project: 'tmobile/templates'
    ref: tmo/master
    file: '/gitlab-ci/.tmo.job.mavenPublish.gitlab-ci.yml'

  - project: 'tmobile/templates'
    ref: tmo/master
    file: '/gitlab-ci/.tmo.job.docker.gitlab-ci.yml'

stages:
  - tmo
  - build
  - test
  - publish
  - package

variables:
    # Common Parameters (used by build and deploy)
    APP_VERSION: "1.0"
    BUILD_NUMBER: "$CI_COMMIT_SHORT_SHA"
    DOCKER_BUILD_ARGS: "--build-arg JAR_FILE=${CI_PROJECT_NAME}-${APP_VERSION}-$CI_COMMIT_SHORT_SHA.jar"

    # Maven Parameters
    MAVEN_PUBLISH_ARGS: "-Dfile=target/${CI_PROJECT_NAME}-${APP_VERSION}-$CI_COMMIT_SHORT_SHA.jar"
    MAVEN_VERSION: "3.6.1-jdk-8"

    # Spring Parameters
    SPRING_PROFILES_ACTIVE: gitlab-ci

```
{{%/expand%}}
### 7 - Push your changes!
{{%expand%}}
```bash
git status
git add -A
git commit -m "replacing jobs with imported t-mobile jobs"
git push origin $LOCAL_BRANCH
```
{{%/expand%}}
### 8. Verifying your pipeline works!
{{%expand%}}
We pushed some changes and verified our pipeline works in the last exercise. Its time to do that again.

Head over to https://gitlab.com/tmobile/cdp/contino/dojo-sample-app and visit your branch to see your pipeline in
action! If you have issues, please reach out.

![Pipelines page](/cdp/dojo/images/exercises/exercise7-verifying-pipeline.png)


{{%/expand%}}
### A quick brief on Gitlab CI Syntax
{{% expand %}}
Pipeline configuration begins with jobs. Jobs are the most fundamental element of a `.gitlab-ci.yml` file.

Jobs are:-   
 - Defined with constraints stating under what conditions they should be executed.   
 - Top-level elements with an arbitrary name and must contain at least the
 [`script`](https://docs.gitlab.com/ee/ci/yaml/#script)clause.   
 - Not limited in how many can be defined.

For example:    
```yaml
job1:
      script: "execute-script-for-job1"
job2:
      script: "execute-script-for-job2"
```

The above example is the simplest possible CI/CD configuration with two separate jobs, where each of the jobs executes
a different command.

Of course a command can execute code directly (`./configure;make;make install`) or run a script (`test.sh`) in the
repository. Jobs are picked up by[Runners](https://docs.gitlab.com/ee/ci/runners/README.html)and executed within
the environment of the Runner.



### Unavailable names for [jobs](https://docs.gitlab.com/ee/ci/yaml/#unavailable-names-for-jobs "Permalink")
Each job must have a unique name, but there are a fewreserved`keywords`that cannot be used as job names:
-   `image`
-   `services`
-   `stages`
-   `types`
-   `before_script`
-   `after_script`
-   `variables`
-   `cache`
-   `include`


### A Gitlab-CI keyword quick-reference

| Keyword | Description |
| --- | --- |
| [`script`](https://docs.gitlab.com/ee/ci/yaml/#script) | Shell script which is executed by Runner. | [`image`](https://docs.gitlab.com/ee/ci/yaml/#image) | Use docker images. Also available:`image:name`and`image:entrypoint`. |
| [`services`](https://docs.gitlab.com/ee/ci/yaml/#services) | Use docker services images. Also available:`services:name`,`services:alias`,`services:entrypoint`, and`services:command`. |
| [`before_script`](https://docs.gitlab.com/ee/ci/yaml/#before_script-and-after_script) | Override a set of commands that are executed before job. |
| [`after_script`](https://docs.gitlab.com/ee/ci/yaml/#before_script-and-after_script) | Override a set of commands that are executed after job. |
| [`stages`](https://docs.gitlab.com/ee/ci/yaml/#stages) | Define stages in a pipeline. |
| [`stage`](https://docs.gitlab.com/ee/ci/yaml/#stage) | Defines a job stage (default:`test`). |
| [`only`](https://docs.gitlab.com/ee/ci/yaml/#onlyexcept-basic) | Limit when jobs are created. Also available:[`only:refs`,`only:kubernetes`,`only:variables`, and`only:changes`](https://docs.gitlab.com/ee/ci/yaml/#onlyexcept-advanced). |
| [`except`](https://docs.gitlab.com/ee/ci/yaml/#onlyexcept-basic) | Limit when jobs are not created. Also available:[`except:refs`,`except:kubernetes`,`except:variables`, and`except:changes`](https://docs.gitlab.com/ee/ci/yaml/#onlyexcept-advanced). |
| [`rules`](https://docs.gitlab.com/ee/ci/yaml/#rules) | List of conditions to evaluate and determine selected attributes of a job, and whether or not it is created. May not be used alongside`only`/`except`. |
| [`tags`](https://docs.gitlab.com/ee/ci/yaml/#tags) | List of tags which are used to select Runner. |
| [`allow_failure`](https://docs.gitlab.com/ee/ci/yaml/#allow_failure) | Allow job to fail. Failed job doesn’t contribute to commit status. |
| [`when`](https://docs.gitlab.com/ee/ci/yaml/#when) | When to run job. Also available:`when:manual`and`when:delayed`. |
| [`environment`](https://docs.gitlab.com/ee/ci/yaml/#environment) | Name of an environment to which the job deploys. Also available:`environment:name`,`environment:url`,`environment:on_stop`, and`environment:action`. |
| [`cache`](https://docs.gitlab.com/ee/ci/yaml/#cache) | List of files that should be cached between subsequent runs. Also available:`cache:paths`,`cache:key`,`cache:untracked`, and`cache:policy`. |
| [`artifacts`](https://docs.gitlab.com/ee/ci/yaml/#artifacts) | List of files and directories to attach to a job on success. Also available:`artifacts:paths`,`artifacts:expose_as`,`artifacts:name`,`artifacts:untracked`,`artifacts:when`,`artifacts:expire_in`,`artifacts:reports`, and`artifacts:reports:junit`.    In GitLab[Enterprise Edition](https://about.gitlab.com/pricing/), these are available:`artifacts:reports:codequality`,`artifacts:reports:sast`,`artifacts:reports:dependency_scanning`,`artifacts:reports:container_scanning`,`artifacts:reports:dast`,`artifacts:reports:license_management`,`artifacts:reports:performance`and`artifacts:reports:metrics`. |
| [`dependencies`](https://docs.gitlab.com/ee/ci/yaml/#dependencies) | Restrict which artifacts are passed to a specific job by providing a list of jobs to fetch artifacts from. |
| [`coverage`](https://docs.gitlab.com/ee/ci/yaml/#coverage) | Code coverage settings for a given job. |
| [`retry`](https://docs.gitlab.com/ee/ci/yaml/#retry) | When and how many times a job can be auto-retried in case of a failure. |
| [`timeout`](https://docs.gitlab.com/ee/ci/yaml/#timeout) | Define a custom job-level timeout that takes precedence over the project-wide setting. |
| [`parallel`](https://docs.gitlab.com/ee/ci/yaml/#parallel) | How many instances of a job should be run in parallel. |
| [`trigger`](https://docs.gitlab.com/ee/ci/yaml/#trigger-premium) | Defines a downstream pipeline trigger. |
| [`include`](https://docs.gitlab.com/ee/ci/yaml/#include) | Allows this job to include external YAML files. Also available:`include:local`,`include:file`,`include:template`, and`include:remote`. |
| [`extends`](https://docs.gitlab.com/ee/ci/yaml/#extends) | Configuration entries that this job is going to inherit from. |
| [`pages`](https://docs.gitlab.com/ee/ci/yaml/#pages) | Upload the result of a job to use with GitLab Pages. |
| [`variables`](https://docs.gitlab.com/ee/ci/yaml/#variables) | Define job variables on a job level. |
| [`interruptible`](https://docs.gitlab.com/ee/ci/yaml/#interruptible) | Defines if a job can be canceled when made redundant by a newer run. |
| [`resource_group`](https://docs.gitlab.com/ee/ci/yaml/#resource_group) | Limit job concurrency. |


### A Brief overview of stages syntax
`stages` is used to define stages that can be used by jobs and is defined globally.

The specification of stages allows for having flexible multi stage pipelines. The ordering of elements in stages defines the ordering of jobs’ execution:
- Jobs of the same stage are run in parallel.
- Jobs of the next stage are run after the jobs from the previous stage complete successfully.

Let’s consider the following example, which defines 3 stages:

    stages:
      - build
      - test
      - deploy

Things to note:

1. First, all jobs of build are executed in parallel.
1. If all jobs of build succeed, the test jobs are executed in parallel.
1. If all jobs of test succeed, the deploy jobs are executed in parallel.
1. If all jobs of deploy succeed, the commit is marked as passed.
1. If any of the previous jobs fails, the commit is marked as failed and no jobs of further stage are executed.

{{% notice info %}}
There are also two edge cases worth mentioning:
If no stages are defined in .gitlab-ci.yml, then the build, test and deploy are allowed to be used as job’s stage by
default and If a job doesn’t specify a stage, the job is assigned the test stage.
{{% /notice %}}

### `.pre and .post`

The following stages are available to every pipeline:
.pre, which is guaranteed to always be the first stage in a pipeline.
.post, which is guaranteed to always be the last stage in a pipeline.
User-defined stages are executed after .pre and before .post.
The order of .pre and .post cannot be changed, even if defined out of order in .gitlab-ci.yml. For example, the
following are equivalent configuration:

Configured in order:

  stages:
    - .pre
    - a
    - b
    - .post

Configured out of order:

  stages:
    - a
    - .pre
    - b
    - .post

Not explicitly configured:

  stages:
    - a
    - b


### [`stage`](https://docs.gitlab.com/ee/ci/yaml/#stage "Permalink")

A `stage`is defined per-job and relies on [`stages`](https://docs.gitlab.com/ee/ci/yaml/#stages) which is defined
globally. It allows to group jobs into different stages, and jobs of the same `stage` are executed in parallel
(subject to[certain conditions](https://docs.gitlab.com/ee/ci/yaml/#using-your-own-runners)). For example:

  stages:
    - build
    - test
    - deploy

  job 0:
    stage: .pre
    script: make something useful before build stage

  job 1:
    stage: build
    script: make build dependencies

  job 2:
    stage: build
    script: make build artifacts

  job 3:
    stage: test
    script: make test

  job 4:
    stage: deploy
    script: make deploy

  job 5:
    stage: .post
    script: make something useful at the end of pipeline


{{% /expand %}}
