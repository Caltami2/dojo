+++
title = "13. Safely migrating Databases"
date = 2019-12-18T10:58:34-05:00
weight = 213
chapter = true
+++

##### Exercise
# Safely Migrating Databases

#### Outcomes

By the end of this exercise:

1. You will implement [Liquibase](https://www.liquibase.org/) in the spring-boot application.
1. You will successfully migrate a database using example changesets


#### Prerequisites

This exercise assumes that you meet the following Prerequisites

1. You should have completed exercise 11
1. You should know GitLab CI principles
1. You should be familiar with the spring-boot sample application.

---

### 1. Create a new branch
{{%expand%}}
```bash
git checkout exercise-13
git checkout -b exercise-13-YOURNAME
```
{{%/expand%}}
### 2. Adding our dependencies.
{{%expand%}}
First, we need to add a new dependency to our `pom.xml` underneath the first `<dependencies>` tag.

```XML
<dependency>
    <groupId>org.liquibase</groupId>
    <artifactId>liquibase-core</artifactId>
</dependency>
```
{{%/expand%}}
### 3. Making some additions to `application.properties`
{{%expand%}}
Next, we have to tell spring-boot that we want to enable liquibase

Add the following to lines to the file `application.properties` in the directory src/main/resources/
```
spring.liquibase.change-log=classpath:/db/changelog/changelog-master.xml
spring.liquibase.enabled=true
```
{{%/expand%}}
### 4. Time to add our master changelog
{{%expand%}}
In order to tell liquibase how we want to migrate the database, we need to add some a master changelog.
Inside this we will add each of our individual changelogs.

First create the changelog directory by running:

Then add a file called `changelog-master.xml` into the src/main/resources/db/changelog directory by running
the following command:

```bash
vi src/main/resources/db/changelog/changelog-master.xml
```

Copy the following contents into that file.

```XML
<?xml version="1.0" encoding="UTF-8"?>
<databaseChangeLog
        xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns:ext="http://www.liquibase.org/xml/ns/dbchangelog-ext"
        xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.1.xsd
    http://www.liquibase.org/xml/ns/dbchangelog-ext http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-ext.xsd">

    <include file="/db/changelog/changes/create-user-table-changelog-1.sql"/>
    <include file="/db/changelog/changes/insert-user-table-changelog-2.xml"/>
    <include file="/db/changelog/changes/create-employees-table-changelog-3.sql"/>
    <include file="/db/changelog/changes/insert-employees-table-changelog-4.xml"/>

</databaseChangeLog>

```
{{%notice info%}}

We have yet to include those files referenced above. We will be doing so in the next step.

{{%/notice%}}
{{%/expand%}}
### 5. Time to add our individual change-sets.
{{%expand%}}
Verify the change sets files in the changes directory:

{{%attachments title="changesets" style="grey" /%}}

```bash
ls -la src/main/resources/db/changelog/changes
```

![](/cdp/dojo/images/exercises/liquid_base_changes_verfiy.png)
{{%/expand%}}
### 6. Run the application against an in-memory database to verify our migration.
{{%expand%}}
You can do so by running the following in the root of your project.

```bash
export CI_PROJECT_NAME=dojo-sample-app
export APP_VERSION=1.2.3
export BUILD_NUMBER=999

mvn spring-boot:run
```

In the mvn run log you should see Liquidbase logs

![](/cdp/dojo/images/exercises/mvn_run_liquid_base_logs.png)


{{%/expand%}}
### 7. Lets push our changes to our own branches and see our pipeline in action!
{{%expand%}}
Remember, to push your changes to your own branch, you can do the following.

```bash
git add -A
git commit -m "my commit message"
git push origin exercise-13-YOURNAME
```

{{%/expand%}}
### 8. Verifying your pipeline works!
{{%expand%}}
Pushing our changes to gitlab automatically triggers a pipeline. We can see this pipeline in the
ui and monitor it during execution.


Head over to https://gitlab.com/tmobile/cdp/contino/dojo-sample-app and visit your branch to see your
pipeline in action! If you have issues, please reach out.


![Pipelines page](/cdp/dojo/images/exercises/exercise7-verifying-pipeline.png)
{{%/expand%}}
