+++
title = "1. Docker and GitLab"
date = 2019-12-18T10:58:34-05:00
weight = 201
chapter = true
+++
##### Exercise
# Docker and GitLab

#### Before We Start
{{% notice note %}}
If you are just getting started with your On-boarding to GitLab, please head over to [CDP's first steps documentation](https://cdp-getting-started.px-npe1103.pks.t-mobile.com/getting-started/first-step/).
<br/><br/>
Please visit the [CDP Getting Started](https://cdp-getting-started.px-npe1103.pks.t-mobile.com/getting-started/)  page for general information about CDP and for documentation about your first pipeline.
{{% /notice %}}

#### Outcomes

By the end of this exercise:

1. You will have Docker installed on your local machine
2. You will be able to log into GitLab with your T-Mobile credentials
3. You will be a Developer on the appropriate GitLab project for this Dojo
4. You will be prepared to run containers during the dojo



#### Prerequisites


This exercise assumes that you meet the following Prerequisites

1. You should have admin access on your laptop
2. You should have reviewed all provided content

{{% notice note %}}
In order to be able to follow along throughout the entire dojo. You will need to do the following steps to set up docker and gitlab. Firstly, you will need to install docker on your local machine. You will also need to ensure that you are able to access GitLab properly and have fully completed the required onboarding steps for T-Mobile.
{{% /notice %}}

---

### Step 1 - Setting Up Docker
{{%expand%}}
First, you need to identify your operating system. Once you have done that, follow the appropriate steps below.

| Docker installation guide per OS |
| ------ |
| [MacOS](https://docs.docker.com/docker-for-mac/install/) |
| [Windows](https://docs.docker.com/docker-for-windows/install/) |
| [Debian](https://docs.docker.com/docs/install/linux/docker-ce/Debian/) |
| [Fedora](https://docs.docker.com/docs/install/linux/docker-ce/Fedora/) |
| [Ubuntu](https://docs.docker.com/docs/install/linux/docker-ce/Ubuntu/) |
| [Binaries](https://docs.docker.com/docs/install/linux/docker-ce/Binaries/) |
| [Optional Linux post-installation steps](https://docs.docker.com/docs/install/linux/linux-postinstall/) |

{{%/expand%}}
### Step 2 - Getting into GitLab
{{%expand%}}
{{% notice warning %}}
BEFORE YOU CONTINUE ! Please remember that this is a SaaS instance of gitlab.com and contains many other users and companies, please be sensitive to offering permissions and project visibility to just T-Mobile
{{% /notice %}}

| How to log into T-Mobile GitLab |
| ------ |
| **1.** While on VPN, Go [here](https://cdp-ramp.us-west-2.elasticbeanstalk.com) and login with your NTID and password to be automatically added to AD groups. |
| **2.** Register with your user@t-mobile.com e-mail with Gitlab and use your NTID as your username. (If your account was already provisioned, go to reset password) |
| **3.** Setup 2FA (Go to your Settings.Go to Account.Click Enable Two-Factor Authentication.) |
| **4.** Go [here](https://gitlab.com/groups/tmobile/-/saml/sso) to link your gitlab account with your T-Mobile SSO. |
| **5.** Ask your Team Lead/Sub-Group owner to be promoted higher than guest for your sub-group under gitlab.com/tmobile. If you are in a dojo  currently, let your dojo leaders know and they will add you to the appropriate projects. |
{{%/expand%}}
### Step 3 - Docker Dojo App
{{%expand%}}
3.1 Create a personal access token
1. Log in to your GitLab account.
2. Go to your Profile settings.
3. Go to Access tokens. or [click here](https://gitlab.com/profile/personal_access_tokens)
4. Choose a name and optionally an expiry date for the token.
5. Choose write and read repository scopes.
6. Click on Create personal access token.
7. Save the personal access token somewhere safe. Once you leave or refresh
the page, you won't be able to access it again.

![Personal Access token](/cdp/dojo/images/exercises/exercise1-access-token.png)

3.2 Log into docker gitlab registry
1. Run docker login
```bash
docker login registry.gitlab.com
```

2. Use your T-Mobile user name and the Personal Access token you created in 3.1

```
Username: YOUR_USERNAME
Password: YOUR_PERSONAL_ACCESS_TOKEN
Login Succeeded
```

3.2 Pull Docker Dojo container image locally

```bash
docker pull registry.gitlab.com/tmobile/cdp/contino/dojo-docker:prd
```
```bash
Unable to find image 'registry.gitlab.com/tmobile/cdp/contino/dojo-docker:prd' locally
prd: Pulling from tmobile/cdp/contino/dojo-docker
e7c96db7181b: Already exists
f910a506b6cb: Already exists
c2274a1a0e27: Already exists
3acbe52510bc: Pull complete
d727b8391bc8: Pull complete
6a3ee6bb241c: Pull complete
68624e85ef0f: Pull complete
5aef48667f41: Pull complete
Digest: sha256:f88cc96b240a80eef75bc80a79a533c9667855316ae72d3d39ddc591400ec7f4
Status: Downloaded newer image for registry.gitlab.com/tmobile/cdp/contino/dojo-docker:prd
bash-5.0#

```
3.3 Test Run


{{% notice note %}}
Make sure docker is running

Please mount a directory from your local filesystem to the code repo in your docker container. This way you will not lose your work if docker crashes. In this exercise, we will reference this local filesystem directory $DOJO_DIR.
{{% /notice %}}

Docker run should drop you into a bash shell

```bash
docker run -v $DOJO_DIR:/dojo-code/ -v /var/run/docker.sock:/var/run/docker.sock -ti registry.gitlab.com/tmobile/cdp/contino/dojo-docker:prd
```
{{%/expand%}}




## Further Reading - Docker Crash Course

{{%expand%}}

Docker Engine is an open source containerization technology for building and
containerizing your applications. Docker Engine acts as a client-server
application with:
* A server with a long-running daemon process [`dockerd`](/engine/reference/commandline/dockerd/).
* APIs which specify interfaces that programs can use to talk to and
  instruct the Docker daemon.
* A command line interface (CLI) client [`docker`](/engine/reference/commandline/cli/).

The CLI uses Docker APIs to control or interact with the Docker daemon
through scripting or direct CLI commands. Many other Docker applications use the
underlying API and CLI. The daemon creates and manage Docker objects, such as
images, containers, networks, and volumes.

Docker Engine has three types of update channels, **stable**, **test**, and **nightly**:

* **Stable** gives you latest releases for general availability.
* **Test** gives pre-releases that are ready for testing before general availability.
* **Nightly** gives you latest builds of work in progress for the next major release.

For more information, see [Release channels](#release-channels).

## Supported platforms

Docker Engine is available on a variety of Linux platforms, [Mac](/docker-for-mac/install/)
and [Windows](/docker-for-windows/install/) through Docker Desktop, Windows
Server, and as a static binary installation. Find your preferred operating
system below.

### Stable

Year-month releases are made from a release branch diverged from the master
branch. The branch is created with format `<year>.<month>`, for example
`18.09`. The year-month name indicates the earliest possible calendar
month to expect the release to be generally available. All further patch
releases are performed from that branch. For example, once `v18.09.0` is
released, all subsequent patch releases are built from the `18.09` branch.

### Test

In preparation for a new year-month release, a branch is created from
the master branch with format `YY.mm` when the milestones desired by
Docker for the release have achieved feature-complete. Pre-releases
such as betas and release candidates are conducted from their respective release
branches. Patch releases and the corresponding pre-releases are performed
from within the corresponding release branch.

> **Note:**
> While pre-releases are done to assist in the stabilization process, no
> guarantees are provided.

Binaries built for pre-releases are available in the test channel for
the targeted year-month release using the naming format `test-YY.mm`,
for example `test-18.09`.

### Nightly

Nightly builds give you the latest builds of work in progress for the next major
release. They are created once per day from the master branch with the version
format:

    0.0.0-YYYYmmddHHMMSS-abcdefabcdef

where the time is the commit time in UTC and the final suffix is the prefix
of the commit hash, for example `0.0.0-20180720214833-f61e0f7`.

These builds allow for testing from the latest code on the master branch.

> **Note:**
> No qualifications or guarantees are made for the nightly builds.

The release channel for these builds is called `nightly`.

## Support

Docker Engine releases of a year-month branch are supported with patches as needed for 7 months after the first year-month general availability
release.

This means bug reports and backports to release branches are assessed
until the end-of-life date.

After the year-month branch has reached end-of-life, the branch may be
deleted from the repository.

### Backporting

Backports to the Docker products are prioritized by the Docker company. A
Docker employee or repository maintainer will endeavour to ensure sensible
bugfixes make it into _active_ releases.

If there are important fixes that ought to be considered for backport to
active release branches, be sure to highlight this in the PR description
or by adding a comment to the PR.

### Upgrade path

Patch releases are always backward compatible with its year-month version.

### Licensing

Docker is licensed under the Apache License, Version 2.0. See
[LICENSE](https://github.com/moby/moby/blob/master/LICENSE) for the full
license text.

## Reporting security issues

The Docker maintainers take security seriously. If you discover a security
issue, please bring it to their attention right away!

Please DO NOT file a public issue; instead send your report privately
to security@docker.com.

Security reports are greatly appreciated, and Docker will publicly thank you
for it.

## Get started

After setting up Docker, you can learn the basics with
[Getting started with Docker](/get-started/).
{{%/expand%}}
