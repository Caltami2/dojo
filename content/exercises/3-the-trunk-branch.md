+++
title = "3. The Trunk Branch"
date = 2019-12-18T10:58:34-05:00
weight = 203
chapter = true
+++
##### Exercise
# The Trunk Branch

#### Outcomes

By the end of this exercise:

1. You will have downloaded master from the remote repo
2. You will have made a dojo specific trunk branch
3. You will have pushed the dojo branch to the remote repo so other people can use it


#### Prerequisites

This exercise assumes that you meet the following Prerequisites

1. You should have an internet connection and the appropriate dojo docker image installed
2. You have clone and push access to the relevant git repositories
3. You should be familiar with working in the command line and be familar with basic git commands

---

## A few steps to get started.

When you are working on a jira ticket, or change to the code base, it is best practice to make a branch off master or
whatever your Team considers to be your trunk/truth branch.



### Step 1 - Cloning "master"
{{%expand%}}
1. First clone a copy of the master branch of the repo to your local computer, if you have not done so already.

2. cd into the Dojo sample directory, should be /dojo-code/dojo-sample-app/.

{{%  notice warning %}}
Make sure you are on the tmo/master branch. You can use the git branch command.
{{%/notice%}}

```bash
git branch
```
You should see an asterisk next to the master branch. If you do not then checkout then tmo/master branch with the
command.

```bash
git checkout tmo/master
```  

{{% notice note%}}
If you are working off a feature branch, or for some reason your team is already working on a branch off master, make
sure you check that branch out instead.
{{% /notice %}}
{{%/expand%}}


### Step 2 - Creating a branch from "master".
{{% expand %}}
`git checkout` is a command that allows you to checkout a branch from the repository. The `-b` flag tells git to create the branch in the process. Lets call the new branch $DOJO_NAME, the dojo name the team
chose in the introduction.

```bash
git checkout -b $DOJO_NAME
```
{{%/expand%}}


### Step 3 - Pushing the branch.

{{%expand%}}
Right now the dojo branch is only on your personal computer. If you want the branch to be available in the GitLab repo, so other people can use it, you need to push it to the remote repo.

1. Use the following command:

```bash
git push --set-upstream origin $DOJO_NAME
```  

That is how you create a trunk branch and push it to the remote repository so other people can use it.

We are now going to review briefly how to add and commit a file.

2. Open up the file with vim src/main/java/com/tmobile/cdp/contino/dojosampleapp/controller/HelloWorldController.java in your
editor. Add a new commented line below the comment block. Save and exit the file ESC :wq

```bash
// Created by ... on ...
```  

3. Next we want to add the changed file to the local repo and push it to the remote location.

4. Find the files that are modified via the git status command:

```bash
git status
```  

5. Add those files via git add:

```bash
git add src/main/java/com/tmobile/cdp/contino/dojosampleapp/controller/HelloWorldController.java
```  

6. Verify files added via git and recognize status change:
```bash
git status
```

7. When you try and commit a file for the first time, if your git config is not completely set up you will have to
set up your email and username.

```bash
git config --global user.email "you@t-mobile.com"
git config --global user.name "Your GitLab Username/NTID"
```

8. Commit those files to the repo.

```bash
git commit -m "Added a comment"
```  

9. When you are done push this branch to the remote repo.

```bash
git push origin $DOJO_NAME
```  
