+++
title = "4. Your Own Branch"
date = 2019-12-18T10:58:34-05:00
weight = 204
chapter = true
+++
##### Exercise
# Your Own Branch
#### Outcomes

By the end of this exercise:

1. You will have created your own local repo


#### Prerequisites

This exercise assumes that you meet the following Prerequisites

1. You should have an internet connection and the appropriate dojo docker image installed
2. You should know git commands such as branch and checkout
3. You should be familiar with working in the command line

---


## Creating your own branch

This will look similar to exercise 3.

1. First clone a copy of the master branch of the repo to your local computer, if you have not done so already.

2. cd into the Dojo sample directory, should be /dojo-code/dojo-sample-app/.

3. Make sure you are on the feature branch, in this case dojo. You can use the git branch command to see which branch you are on.

```bash
git branch
```

4. You should see an asterisk next to the dojo branch. If you do not then checkout then dojo branch with the command.

```bash
git checkout $DOJO_BRANCH
git fetch
```  

5. Create your local branch off branch dojo.
```bash
git checkout -b $LOCAL_BRANCH
```
