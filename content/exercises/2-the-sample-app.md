+++
title = "2. Sample App"
date = 2019-12-18T10:58:34-05:00
weight = 202
chapter = true
+++

##### Exercise

# Getting to Know the Sample App

#### Outcomes

By the end of this exercise:

1. You will have downloaded the sample application into the Dojo Docker container

#### Prerequisites

This exercise assumes that you meet the following Prerequisites

1. You should have docker installed and gitlab access
1. You should know how to access the docker dojo container
1. You should be familiar with docker commands

{{% notice note %}}
 The Sample Application utilized in this course is built using a series of different tools, languages, and concepts including, but not limited to, Java, Spring-Boot, Maven, YAML, Docker, and various scripting. However, you do not need to have heavy experience in any of the above components to be able to grasp the concepts and follow along. If at any point you come across something confusing or blatantly incorrect, please feel free to reach out in the #dojo slack channel for support.
{{%/ notice %}}


## Step 1 - Running Sample Application
{{%expand%}}
1. Run the docker dojo container locally

    Follow the steps from [Exercise 1](/cdp/dojo/exercises/1-docker-and-gitlab/)

2. Git clone sample application. Make sure you are operating inside the docker container.

```bash
cd /dojo-code/
git clone https://gitlab.com/tmobile/cdp/contino/dojo-sample-app.git
```

You will be prompted to enter your ntid and token. Token here means password.

That's it. You are done!
{{%/expand%}}
##  Further Reading - What is Spring Boot?
Spring Boot provides a good platform for Java developers to develop a stand-alone and production-grade spring application that you can just run. You can get started with minimum configurations without the need for an entire Spring configuration setup. In order to get familiar with spring-boot. Expand the following tutorial by clicking below.

{{%expand%}}

### Advantages

Spring Boot offers the following advantages to its developers

- Easy to understand and develop spring applications
- Increases productivity
- Reduces the development time Goals





### Goals

Spring Boot is designed with the following goals and objectives in mind.

- To avoid complex XML configuration in Spring
- To develop a production ready Spring applications in an easier way
- To reduce the development time and run the application independently
- Offer an easier way of getting started with the application




### Why Spring Boot?

Spring Boot was specifically chosen for the exercises for the following reasons.

- It provides a flexible way to configure Java Beans, XML configurations, and Database Transactions.
- It provides a powerful batch processing and manages REST endpoints without much code.
- In Spring Boot, everything is auto configured; no manual configurations are needed.
- It offers annotation-based spring application
- Eases dependency management
- It includes Embedded Servlet Container
- It allows us to learn about services and real-world development by minimizing the requrired experience to get started.




### How does it work?

Spring Boot uses a few different ways to automate a vast majority of the required pieces needed to produce a web-service. However, there is one key tool that we will use to deploy a secure, REST service that is easy to manage, change, and test. That key tool is the power of Annotations. See Below.

Spring Boot automatically configures your application based on the dependencies you have added to the project by using `@EnableAutoConfiguration` annotation. For example, if MySQL database is on your classpath, but you have not configured any database connection, then Spring Boot auto-configures an in-memory database.

The entry point of the spring boot application is the class contains `@SpringBootApplication` annotation and the main method.

Spring Boot automatically scans all the components included in the project by using `@ComponentScan` annotation.

{{% notice warning %}}

The information below this warning is vital to following allowing in the Dojo. If you have decided to skip over the first fiew parts due to your prior experience. Please take a look through these in order to ensure you get the most out of our in-class opportunity.

{{% /notice %}}




### A quick brief on Spring Boot Starters

Handling dependency management is a difficult task for big projects. Spring Boot resolves this problem by providing a set of dependencies for developers convenience.

For example, if you want to use Spring and JPA for database access, it is sufficient if you include spring-boot-starter-data-jpa dependency in your project.

{{% notice info %}}
Note that all Spring Boot starters follow the same naming pattern spring-boot-starter- *, where * indicates that it is a type of the application.
{{%/notice %}}

#### Examples

Look at the following Spring Boot starters explained below for a better understanding.

Spring Boot Starter Actuator dependency is used to monitor and manage your application. Its code is shown below −

    <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>

Spring Boot Starter Security dependency is used for Spring Security. Its code is shown below −

    <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-security</artifactId>
    </dependency>

Spring Boot Starter web dependency is used to write a Rest Endpoints. Its code is shown below −

    <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-web</artifactId>
    </dependency>

Spring Boot Starter Thyme Leaf dependency is used to create a web application. Its code is shown below −

    <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-thymeleaf</artifactId>
    </dependency>

Spring Boot Starter Test dependency is used for writing Test cases. Its code is shown below −

    <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-test</artifactId>
    </dependency>



### Auto Configuration



Spring Boot Auto Configuration automatically configures your Spring application based on the JAR dependencies you added in the project. For example, if MySQL database is on your class path, but you have not configured any database connection, then Spring Boot auto configures an in-memory database.

For this purpose, you need to add `@EnableAutoConfiguration` annotation or @SpringBootApplication annotation to your main class file. Then, your Spring Boot application will be automatically configured.

Observe the following code for a better understanding

  import org.springframework.boot.SpringApplication;
  import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

  @EnableAutoConfiguration
  public class DemoApplication {
     public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
     }
  }

### Spring Boot Application
The entry point of the Spring Boot Application is the class contains @SpringBootApplication annotation. This class should have the main method to run the Spring Boot application. @SpringBootApplication annotation includes Auto- Configuration, Component Scan, and Spring Boot Configuration.

If you added @SpringBootApplication annotation to the class, you do not need to add the @EnableAutoConfiguration, @ComponentScan and @SpringBootConfiguration annotation. The @SpringBootApplication annotation includes all other annotations.

Observe the following code for a better understanding −

  import org.springframework.boot.SpringApplication;
  import org.springframework.boot.autoconfigure.SpringBootApplication;

    @SpringBootApplication
    public class DemoApplication {
       public static void main(String[] args) {
          SpringApplication.run(DemoApplication.class, args);
       }
    }

### Component Scan

Spring Boot application scans all the beans and package declarations when the application initializes. You need to add the @ComponentScan annotation for your class file to scan your components added in your project.

Observe the following code for a better understanding −

    import org.springframework.boot.SpringApplication;
    import org.springframework.context.annotation.ComponentScan;

    @ComponentScan
    public class DemoApplication {
       public static void main(String[] args) {
          SpringApplication.run(DemoApplication.class, args);
       }
    }
{{%/expand%}}
