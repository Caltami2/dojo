+++
title = "6. Merge Request Workflows"
date = 2019-12-18T10:58:34-05:00
weight = 206
chapter = true
+++

##### Exercise
# Merge Request Workflows

#### Outcomes

By the end of this exercise:

1. You will know how to protect the team's trunk branch
2. You will learn how to set up restrictions on branches



#### Prerequisites

This exercise assumes that you meet the following Prerequisites

1. You should have a gitlab project
2. You should know what a merge conflict is
3. You should be familiar with git merge and other git commands

---

## Guidelines on Merge Request Etiquette

1. Anyone can raise a discussion on anyone else.
2. Debates/Contentious Discussions in MR can be resolved by another approver if necessary.  
3. Simple Corrections, can be resolved by MR author.

## Protected Branch

By default, a protected branch does four simple things:

* It prevents its creation, if not already created, from everybody except users with Maintainer permission.
* It prevents pushes from everybody except users with Allowed permission.
* It prevents anyone from force pushing to the branch.
* It prevents anyone from deleting the branch.

In this Exercise we are going to
* Protect the Dojo Truth Branch
* Restrict direct push access
* Set up code owners

### Step 1 -  Protect Dojo Truth Branch
{{%expand%}}
1. Navigate to your project’s Settings ➔ Repository
2. Scroll to find the Protected branches section.
3. Select "allowed to merge" and "allowed to push"
4. From the Branch dropdown menu, select the branch you want to protect.

In the screenshot below, we chose the develop branch but you'll want to protect the Dojo Truth Branch.

![Protect Branch Page](/cdp/dojo/images/exercises/protected_branches_page.png)


5. Restrict direct push access, In the drop down menu for Allowed to Merge select developers and maintainers and for
Allowed to Push, select No One

![Who can push](/cdp/dojo/images/exercises/protected_branches_can_push.png)

6. Click Protect

7. Now, the protected branch will appear in the “Protected branches” list.

![Protected Branch List](/cdp/dojo/images/exercises/protect_branch_list.png)
{{%/expand%}}
### Step 2 -  Setting up code owners
{{%expand%}}

You can use a CODEOWNERS file to specify users or shared groups that are responsible for certain files in a repository.

You can choose and add the CODEOWNERS file in three places:

* To the root directory of the repository
* Inside the .gitlab/ directory
* Inside the docs/ directory

1. One person from the Dojo class collect Users ID for everyone in the dojo
2. Create a Branch called CODEOWNERS

```bash
git checkout $DOJO_BRANCH
git checkout -b CODEOWNERS
```

3. Create file CODEOWNERS. You can use vi CODEOWNERS to open the file.

For Example:
```markdown
CODEOWNERS @yourNTID @otherNTIDs
```

4. Commit CODEOWNERS file

```bash
git add CODEOWNERS
git commit -m "Creating CODEOWNERS files"
git push origin CODEOWNERS
```

5. Create a Merge request for the team to approve, like in exercise 5.

6. Merge into Dojo team trunk branch

Gitlab References

https://docs.gitlab.com/ee/user/project/protected_branches.html

https://docs.gitlab.com/ee/user/project/code_owners.html
{{%/expand%}}
