+++
title = "9. Pipelines and Stages"
date = 2019-12-18T10:58:34-05:00
weight = 209
chapter = true
+++
##### Exercise
# Pipelines and Stages

#### Outcomes

By the end of this exercise:

1. You will add stages to your gitlab-ci.yml
2. You will deploy your new stages to GitLab
3. You will create a pipeline


#### Prerequisites

This exercise assumes that you meet the following Prerequisites

1. You have a gitlab-ci.yml file and have basic undertanding of the gitlab-ci yml format
2. You should know how to navigate the GitLab GUI
3. You should be familiar with CI/CD concepts and terminologies

---

## Pipelines and Stages

With GitLab CI/CD we can string together jobs and create a pipeline. In this example you will learn how to do this.

1. In the root directory of your repo open your .gitlab-ci.yml in your text editor. If this file is in your .gitignore, then remove it from .gitignore, refetch the repo and open the file. Remember to add it back to your .gitignore file at the end of the exercise. You can use vi .gitlab-ci.yml to edit this file.

2. In your .gitlab-ci.yml you will see a number of jobs and inhereted jobs. Our yml file has inhereted the test and build jobs.

4. Stages is the main way to define a pipeline. We will add stages and two jobs to stages. In your yml file write or edit it to have the following lines:
```bash
stages:
  - build
  - test
```

5. Once you are done with this change, save and close the file. Add the code to the repo and push it to the remote repository.

```bash
git add .gitlab-ci.yml
git commit -m "edited a pipeline to this project"
git push origin $LOCAL_BRANCH
```

6. Now you have created a new pipeline. We can monitor the pipeline via the GitLab GUI.  Go to your GitLab repo, select CI/CD from the tab bar on the left, and then select Pipelines. You should see your pipeline at the top with 2 circles representing the 2 jobs (or stages) we added to the pipeline. We can use this interface to rerun jobs, look at the terminal for failed jobs, download artifacts, or run the pipeline.

![Pipeline Visualization](/cdp/dojo/images/exercises/exercise9-pipeline.png)
