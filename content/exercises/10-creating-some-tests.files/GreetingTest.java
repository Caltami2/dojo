package com.tmobile.cdp.contino.dojosampleapp.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GreetingTest {

    @Test
    public void greetingGettersTest(){
        //Arange
        Greeting greeting = new Greeting(12, "Hello");

        //Act
        // No act here, the class returns what we tell too

        //Assest
        assertEquals( greeting.getId(), 12);
        assertEquals( greeting.getGreeting(), "Hello");
    }
}
