+++
title = "10. Creating Some Tests"
date = 2019-12-18T10:58:34-05:00
weight = 210
chapter = true
+++
##### Exercise
# Creating Some Tests

#### Outcomes

By the end of this exercise:

1. You will create unit tests for the sample application
2. You will understand the need for unit tests


#### Prerequisites

This exercise assumes that you meet the following Prerequisites

1. You should have the sample application cloned in your docker environment
2. You should know how to run the docker exercise container
3. You should be familiar with units tests

---

{{% notice warning %}}
Continue working on your branch you created in Exercise 7
{{% /notice %}}

1. Write a new test for Greeting Class

{{%attachments title="Greeting Test Java file" style="grey" pattern="GreetingTest.java"/%}}

Download the GreetingTest.java locally

In a new terminal window get the name of the running Dojo Docker container

```bash
○ → docker ps
CONTAINER ID        IMAGE                                                     COMMAND             CREATED             STATUS              PORTS               NAMES
9cf6ac2e310d        registry.gitlab.com/tmobile/cdp/contino/dojo-docker:prd   "/bin/bash"         19 minutes ago      Up 19 minutes                           ecstatic_kepler
```

Then use docker to copy in the files

```bash
docker cp GreetingTest.java 9cf6ac2e310d:/dojo-code/dojo-sample-app/src/test/java/com/tmobile/cdp/contino/dojosampleapp/model/
```

2. Run it inside the Dojo Docker container

```bash
export CI_PROJECT_NAME=dojo-sample-app
export APP_VERSION=1.2.3
export BUILD_NUMBER=999
mvn test
```

3. Commit it

```bash
git add src/test/java/com/tmobile/cdp/contino/dojosampleapp/model/GreetingTest.java
git commit -m "added greeting tests"
git push origin $LOCAL_BRANCH
```
