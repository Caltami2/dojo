+++
title = "7. Your first build"
date = 2019-12-18T10:58:34-05:00
weight = 207
chapter = true
+++

##### Exercise
# Your First Build

#### Outcomes

By the end of this exercise:

1. You will know how the `.gitlab-ci.yml` file is structured and what it does.
1. You will create your own `.gitlab-ci.yml` that builds out the sample app.
1. You will push your `.gitlab-ci.yml` build to GitLab and verify your pipeline works.


#### Prerequisites

This exercise assumes that you meet the following prerequisites:

1. You should have completed exercise 6 in this Dojo.
1. You should know what Spring-Boot is and how the apps are built.
1. You should be familiar with Gitlab CI principles.

---
### Before you get started

On the Sample app checkout exercise-6 branch as a starting point for all our exercises.

```bash
git fetch
git checkout exercise-6
git checkout -b $LOCAL_BRANCH
```

### 1. Start by adding a Dockerfile.
{{%expand%}}
Let's start by creating a new file called `Dockerfile` in the root directory of our project.

```bash
  vi Dockerfile
```

Then, paste in the contents below.

    FROM openjdk:8u111-jdk-alpine
    VOLUME /tmp
    ARG JAR_FILE
    ENV JAR_FILE=${JAR_FILE}
    ADD /target/${JAR_FILE} app.jar
    ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]

Then save the file and exit. You can do this by : ESC :wq

Read below for a deeper dive into the contents of the Dockerfile.
{{% notice info %}}

  - `FROM` keyword defines the base Docker image of our container.
  - We chose OpenJDK installed on Alpine Linux which is a lightweight Linux distribution.
  - `VOLUME` instruction creates a mount point with the specified name and marks it as holding externally mounted volumes
  from the native host or other containers.
  - `ADD` copies the executable JAR generated during the build to the container root directory.
  - `ENTRYPOINT` defines the command to execute when the container is started.
  - Since Spring Boot produces an executable JAR with embedded Tomcat, the command to execute is simply java -jar app.jar.
  - The additional flag java.security.edg=file:/dev/./urandom is used to speed up the application start-up and avoid
  possible freezes.
  - By default, Java uses /dev/random to seed its SecureRandom class which is known to block if its entropy pool is empty.

{{% /notice %}}
{{%/expand%}}
### 2. Now that its containerized... Lets create our `.gitlab-ci.yml`
{{%expand%}}
Now that it is in a container, we can proceed with getting our gitlab-ci build running. In order to do that, we must
first create the file.

While you are in the **root** directory of the dojo-sample-app, run the following command to create a file called `.gitlab-ci.yml`

    vi .gitlab-ci.yml

We will now add some contents to this empty document.

{{%/expand%}}
### 3. Declaring the our images and services.
{{%expand%}}
In the very top of your `.gitlab-ci.yml` add the following image and service definitions.

    image: docker:latest
    services:
    - docker:dind

{{% notice info %}}

The [GitLab Runner](https://docs.gitlab.com/ee/ci/runners/README.html) can
[use Docker images](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html) to support our pipelines.
he [`image`element](https://docs.gitlab.com/ee/ci/yaml/#image-and-services) defines the name of the Docker image
we want to use. Valid images are those hosted in the local Docker Engine or on [Docker Hub](https://hub.docker.com/).
The `services` element defines additional Docker images which are linked to the main container. In our case the main
container is a plain Docker image while the linked container is enabled for running Docker in Docker.

{{% /notice %}}

{{%/expand%}}
### 4. Declaring our variables
{{%expand%}}
The following variables are going to be used throughout our exercises, for now, go ahead and add them to your
`.gitlab-ci.yml`. Their use will become clear in the following exercises.

```yaml
variables:
    # Common Parameters (used by build and deploy)
    APP_VERSION: "1.0"
    BUILD_NUMBER: "$CI_COMMIT_SHORT_SHA"
    DOCKER_BUILD_ARGS: "--build-arg JAR_FILE=${CI_PROJECT_NAME}-${APP_VERSION}-$CI_COMMIT_SHORT_SHA.jar"

    # Maven Parameters
    MAVEN_VERSION: "3.6.1-jdk-8"

    # Spring Parameters
    SPRING_PROFILES_ACTIVE: gitlab-ci
```


This is the definition of [`variables`](https://docs.gitlab.com/ee/ci/yaml/#variables) to be set on our build
environment. The `DOCKER_DRIVER` signals the Docker Engine which storage driver to use. We use `overlay` for
[performance reasons](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#using-the-overlayfs-driver).
The `SPRING_PROFILES_ACTIVE` is very useful when dealing with Spring Boot applications. It activates
[Spring Profiles](http://docs.spring.io/autorepo/docs/spring-boot/current/reference/html/boot-features-profiles.html),
which provide a way to segregate parts of our application configuration and make it available only in certain
environments. For instance, we can define different database URIs per environment, e.g. `localhost` when running on
the developer machine and `mongo` when running within GitLab CI

{{% /expand %}}


### 5. Lets take a look at what we have so far
{{%expand%}}
Your current Gitlab-CI should look like this.
```yaml
image: docker:latest
services:
  - docker:dind

variables:
    # Common Parameters (used by build and deploy)
    APP_VERSION: "1.0"
    BUILD_NUMBER: "$CI_COMMIT_SHORT_SHA"
    DOCKER_BUILD_ARGS: "--build-arg JAR_FILE=${CI_PROJECT_NAME}-${APP_VERSION}-$CI_COMMIT_SHORT_SHA.jar"

    # Maven Parameters
    MAVEN_VERSION: "3.6.1-jdk-8"

    # Spring Parameters
    SPRING_PROFILES_ACTIVE: gitlab-ci
```

So far we have :

-  Defined the image that our runner will use to build our `Dockerfile`
-  Defined some variables that we will consume later on in the build process
-  We have **declared** two stages in our pipeline. These stages will still need to be referenced in a **job**

If your `.gitlab-ci.yml` looks like the example above, then you can proceed the fun parts.

{{%/expand%}}
### 6. Lets define a job : maven-build.
{{%expand%}}
Our build stage will be the stage in which we build our spring-boot application. We will be using the dependency
manager maven to build and package our java application.

Lets add our maven job.
```yaml
maven-build:
  stage: build
  image: maven:${MAVEN_VERSION}
  script: "mvn package -B -Dfile=target/${CI_PROJECT_NAME}-${APP_VERSION}-$CI_COMMIT_SHORT_SHA.jar"
  artifacts:
    paths:
      - target/*.jar
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - mvn/
```

{{%/expand%}}
### 7. Lets define a job : docker-package.
{{%expand%}}
Add the following to your gitlab-ci after the maven job we just added above.

```yaml
docker-package:
  stage: package
  script:
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  - eval "docker build -t $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA $DOCKER_BUILD_ARGS ."
  - docker push  "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA"
```
{{%/expand%}}
### 8. Lets define our stages.
{{%expand%}}
In order to tell GitLab which job to run first we must define our stages.

You will see that we have defined which stage that each job should run in, but not what order the stages should be in.

Add the following code right after line 4 in your `.gitlab-ci.yml`

```yaml
stages:
  - build
  - package
```

Next save and exit.
{{%/expand%}}
### 9. Lets push our changes to our own branches and see our pipeline in action!
{{%expand%}}
Remember, to push your changes to your own branch, you can do the following.

```bash
git add Dockerfile
git add .gitlab-ci.yml
git status
git commit -m "my commit message"
git push origin $LOCAL_BRANCH
```

As a quick reference, this is what your `.gitlab-ci.yml` should look like right now.

```yaml
image: docker:latest
services:
  - docker:dind

stages:
  - build
  - package

variables:
  # Common Parameters (used by build and deploy)
  APP_VERSION: "1.0"
  BUILD_NUMBER: "$CI_COMMIT_SHORT_SHA"
  DOCKER_BUILD_ARGS: "--build-arg JAR_FILE=${CI_PROJECT_NAME}-${APP_VERSION}-$CI_COMMIT_SHORT_SHA.jar"

  # Maven Parameters
  MAVEN_VERSION: "3.6.1-jdk-8"
  # Spring Parameters
  SPRING_PROFILES_ACTIVE: gitlab-ci

maven-build:
  stage: build
  image: maven:${MAVEN_VERSION}
  script: "mvn package -B -Dfile=target/${CI_PROJECT_NAME}-${APP_VERSION}-$CI_COMMIT_SHORT_SHA.jar"
  artifacts:
    paths:
      - target/*.jar
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - mvn/


docker-package:
  stage: package
  script:
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  - docker build . -t "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA" $DOCKER_BUILD_ARGS
  - docker push  "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA"
```

{{%/expand%}}
### 10. Change link to direct to pipeline.
{{%expand%}}
Pushing our changes to gitlab automatically triggers a pipeline. We can see this pipeline in the ui and monitor it
during execution.


Head over to https://gitlab.com/tmobile/cdp/contino/dojo-sample-app and visit your branch to see your pipeline in
action! If you have issues, please reach out.

![Pipelines page](/cdp/dojo/images/exercises/exercise7-verifying-pipeline.png)

{{%/expand%}}

## More details, reading, and information about GitLab

{{% expand %}}

Throughout these exercises, we will make changes to achieve our ultimate goal of creating a Continuous Delivery
pipeline to deploy a Spring Boot app with GitLab CI and Kubernetes. Continuous integration, continuous deployment
and continuous delivery are increasingly popular topics among modern development teams. Together they enable a
team to build, test and deploy the code at any commit. The main benefit of these approaches is the ability to release
more quality code more frequently through the means of automated pipelines. The tough part is building such pipelines.
There is a myriad of tools available which we would need to choose, learn, install, integrate, and maintain. Recently,
I literally fell in love with GitLab! It offers a fully featured ecosystem of tools which enable us to create an
automated pipeline in minutes! From source control to issue tracking and CI, we find everything under one roof,
fully integrated and ready to use. In this tutorial, we will create a Spring Boot application built, tested, and
eployed with GitLab CI on a Kubernetes cluster. Spring Boot is the leading microservice chassis for Java. It allows
a developer to build a production-grade stand-alone application, like a typical CRUD application exposing a RESTful
API, with minimal configuration, reducing the learning curve required for using the Spring Framework drastically.
Kubernetes is an open- source container orchestrator inspired by Google Borg that schedules, scales and manages
containerized applications.

## Creating a continuous delivery pipeline with GitLab

While our code is now safe on GitLab, we still need to automate its integration and deployment. We need to verify each
commit with an automated build and set of tests in order to discover issues as early as possible and, if the build is
successful, deploy to a target environment. A few years ago, our only option was to install, configure and maintain a CI.

```bash
  2016-12-02 22:41:14.376 INFO 10882 --- **[** main] s.b.c.e.t.TomcatEmbeddedServletContainer : Tomcat started on port
  2016-12-02 22:41:14.420 INFO 10882 --- **[** main] com.example.ActuatorSampleApplication : Started ActuatorSampleApplication

  ~$ curl [http://localhost:8080/health](http://localhost:8080/health)
  **{** "status":"UP","diskSpace": **{** "status":"UP","total":981190307840,"free":744776503296,
```

Servers like Jenkins and possibly automate our deployment with a set of bash scripts. While the number of options has
grown significantly, whether hosted or on the cloud, we still need to find a way to integrate our source control system
with the CI Server of our choice. Not anymore though! GitLab has fully integrated CI and CD Pipelines in its offering,
allowing us to build, test and deploy our code with ease. For the purpose of this tutorial we will deploy to the Google
Cloud Container Engine which is a cluster management and orchestration system built on the open source Kubernetes.
Kubernetes is supported by all main cloud providers and can be easily installed on any Linux server in minutes.
That said, we will be able to re-use this configuration virtually on any environment running Kubernetes.

Before we can proceed to the creation of the pipeline, we need to add a couple of files to our repository to package
our application as a Docker container and to describe the target deployment in Kubernetes terms. Packaging a Spring
Boot application as a Docker container. Let's start by creating the Dockerfile in the root directory of our project.

The FROM keyword defines the base Docker image of our container. We chose OpenJDK installed on Alpine Linux which is a
lightweight Linux distribution. The VOLUME instruction creates a mount point with the specified name and marks it as
holding externally mounted volumes from the native host or other containers. ADD copies the executable JAR generated
during the build to the container root directory. Finally ENTRYPOINT defines the command to execute when the container
is started. Since Spring Boot produces an executable JAR with embedded Tomcat, the command to execute is simply
java -jar app.jar. The additional flag java.security.edg=file:/dev/./urandom is used to speed up the application
start-up and avoid possible freezes. By default, Java uses /dev/random to seed its SecureRandom class which is known to
block if its entropy pool is empty.


    FROM openjdk:8u111-jdk-alpine
    VOLUME /tmp
    ADD /target/dojosampleapp-0.0.1-SNAPSHOT.jar app.jar
    ENTRYPOINT **[** "java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"


## Time to commit again and we are ready to define our GitLab CI pipeline.

    git add deployment.yml
    git commit -m "Adds Kubernetes Deployment definition"
    git push origin $LOCAL_BRANCH

## Creating the GitLab CI pipeline
In order to make use of GitLab CI we need to add the .gitlab-ci.yml configuration file to the root directory of our
repository. This file is used by GitLab Runners to manage our project's builds and deployments. Therein we can define
an unlimited number of Jobs and their role in the whole build lifecycle.

#### Let's break the file in pieces to understand what is going on.

#### Image and Services

    image: docker:latest
    services:

    - docker:dind

    stages:

    - build
    - package
    - deploy

    maven-build:
    image: maven:3-jdk-8
    stage: build
    script: "mvn package -B"
    artifacts:
    paths:

    - target/*.jar

    docker-build:
    stage: package
    script:

    - docker build -t registry.gitlab.com/tmobile/cdp/dojo-sample-app
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
    - docker push registry.gitlab.com/tmobile/cdp/dojo-sample-app/dojo-sample-app




    The maven-build job

        maven-build:
        image: maven:3-jdk-8
        stage: build
        script: "mvn package -B"
        artifacts:
        paths:
        - target/*.jar


As previously said, the maven-test-jdk-8 job runs in parallel with the maven-  build. Hence, it does not have an impact
on the pipeline execution time.  The script is a shell command to be executed by the GitLab Runner. The
mvn package -B triggers a non-interactive Maven build up to the package  phase. This phase is specific to the Maven
build lifecycle and it includes  also the validate, compile and test phases. That means that our Maven  project will
be validated, compiled and (unit) tested as well. Tests are to be  included in the src/test/java folder. In our
specific case, Spring Initializr  has already created a unit test which verifies that the application context  loads
without errors. We are free to add as many unit tests as we like.  Finally, the package phase creates the executable JAR.


These are files or directories that are attached to the build after success and made downloadable from the UI in the
Pipelines screen.

#### The docker-build job

  docker-build:
  stage: package
  script:
  - docker build -t registry.gitlab.com/tmobile/cdp/dojo-sample-app
  - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
  - docker push registry.gitlab.com/tmobile/cdp/dojo-sample-app

The docker-build job packages the application into a Docker container. We define package as the build stage since we
need the maven-build job to produce the executable JAR beforehand.

The scripts are a typical sequence of docker commands used to build an image, log in to a private registry and
push the image to it. We will be pushing images to the GitLab Container Registry.

The `$CI_BUILD_TOKEN` is a pre-defined variable which is injected by GitLab CI into our build environment automatically.
It is used to log in to the GitLab Container Registry. For a complete list of pre-defined variables, have a look at the
variables documentation.

{{% /expand %}}
