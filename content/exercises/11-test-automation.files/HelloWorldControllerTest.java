package com.tmobile.cdp.contino.dojosampleapp.controller;

import com.tmobile.cdp.contino.dojosampleapp.model.Greeting;
import org.junit.Assert.*;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;


/**
 * Created by SSajjad1 on 3/8/2017.
 */
public class HelloWorldControllerTest {

    @Test
    public void testController() {
        HelloWorldController controller = new HelloWorldController();
        Greeting greeting = controller.sayHello("Moto");
        assertNotNull(greeting);
        assertEquals(greeting.getGreeting(), "Hello, Moto!");
    }
}
