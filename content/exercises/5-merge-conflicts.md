+++
title = "5. Merge Conflicts"
date = 2019-12-18T10:58:34-05:00
weight = 205
chapter = true
+++
##### Exercise
# Merge Conflicts

#### Outcomes

By the end of this exercise:

1. You will understand how merge conflicts occur
2. You will understand how to resolve a merge conflict

#### Prerequisites

This exercise assumes that you meet the following Prerequisites

1. You should know how to create a branch in git
2. You should know how to add and commit file in git
3. You should be familiar with git commands such as log

---

## Merge Conflicts

A Merge Conflicts arises when you merge your code into another repo and the other repo has a change on a line that
conflicts with a change you made.

{{% notice note %}}
For example if line 10 has "HELLO WORLD" in the parent repo, and line 10 in your repo says "GOODNIGHT MOON".  
{{% /notice %}}

Once you have made changes to your personal or dev repo you want to merge it to the parent branch. We are going to use
rebase in order to merge changes from master into changes on a child branch. When you merge, often times there will be
merge conflicts.

As a reminder the repo we are working with is

```bash
tmobile/cdp/contino/dojo-sample-app
```

### Fabricating a Merge Conflict

Now we are going to trigger a merge conflict. Make sure you are on your local branch. If you are not then move to
your local branch.

```bash
git branch
git checkout $LOCAL_BRANCH
```

### Step 1 - Changing the greeting
{{%expand%}}
Open src/main/java/com/tmobile/cdp/contino/dojosampleapp/controller/HelloWorldController.java in vi.

Edit the Template variable and change the word Hello to something else. Close and save this file. Add it to your repo.

```bash
git add src/main/java/com/tmobile/cdp/contino/dojosampleapp/controller/HelloWorldController.java
git commit -m "changed greeting"
```
{{%/expand%}}
### Step 2 - Changing the greeting in the parent branch
{{%expand%}}
Check out the parent branch and change this same file. This time change Hello to Goodbye, when your are done save
and commit the file.

```bash
git checkout $DOJO_BRANCH
.. edit file ..
git add $FILENAME
git commit -m "Merge Conflict: this should be very descriptive"
```
{{%/expand%}}
### Step 2 - Merging your local branch into parent branch
{{%expand%}}
Merge your local branch into the parent branch

```bash
git status
git checkout $LOCAL_BRANCH
git rebase -i $DOJO_BRANCH
```
How do you know you have a merge conflict?
You will now get the following error:

``` text
Auto-merging call-for-submissions.md
CONFLICT (content): Merge conflict in $FILENAME
Automatic merge failed; fix conflicts and then commit the result.
```

What this means is that in filename there is a merge conflict.
{{%/expand%}}
### Step 3 - Resolving the Conflict
{{%expand%}}
1. Perform a Git Status to view the merge conflicted file.

```bash
git status
```

2. Open the CONFLICT file(s) in your text editor. Here it will be

```bash
vi src/main/java/com/tmobile/cdp/contino/dojosampleapp/controller/HelloWorldController.java.
```

3. In the file search for all the occurances of "<<<<<<< HEAD". This marks the conflicts between the two branches in
the file. HEAD refers in this case to the master branch.

The format is:
```
<<<<<<< PARENT BRANCH
LINE OF CODE IN PARENT THAT CONFLICTS
====
LINE OF CODE IN BRANCH YOU ARE MERGING THAT CONFLICTS
>>>>>>> BRANCH TO MERGE NAME
```

{{% notice note %}}
The parent branch is referred to as HEAD, because this is where the top of the current branch is pointing to. You are
on the parent branch, and the branch is pointing to a commit called HEAD (often the lastest commit).
{{% /notice %}}

There are other tools such as gitkracken that you can use to view and edit merge conflicts, but a good reason to keep
MRs small is to make it easy to resolve conflicts without too many tools.

4. Between HEAD and your branch hash, put the code you want to use. For this exercise use the code from your local
branch. Delete everything else including the lines with HEAD and your branch hash.

5. When you are done with your edits you need add and commit the updated file:

```bash
git add $FILENAME
git commit -m "Merge Conflict Resolved: what was your code change" $FILENAME
```

6. Since we are rebasing you are going to want to continue your rebase.
If you do git branch at this point, you will see that you are detached. In order to get back on your branch you must
continue the rebase.

```bash
git rebase --continue
```

7. Your merge is now complete. You can continue rebasing, or if this was a one time merge, you can continue working on
your code. Run git log you will see your commit at HEAD of the parent branch to verify that your change is actually in
the correct place.

```bash
git log
```
### Step 4 - Pushing to GitLab
{{%expand%}}
1. Now we are going to push out local repo to the GitLab.
```bash
git push --set-upstream origin $LOCAL_BRANCH
```

2. Go to the GitLab Merge Request screen and open a merge request for the repo
https://gitlab.com/tmobile/cdp/contino/dojo-sample-app. The source branch should be your local repo, and the
destination branch should be dojo. Click compare branches and continue.

![MR](/cdp/dojo/images/exercises/exercise5-mr.png)
![New MR](/cdp/dojo/images/exercises/exercise5-mr-new.png)

3. Now you are on the new merge request page. Enter in a description for your merge request and click submit merge
request.  Now members of your team can comment on your MR.

{{% notice note %}}
NOTE: As .gitlab-cy.yml is invalid the Merge Request will not be able to be performed.
{{% /notice %}}
{{%/expand%}}
