+++
title = "Agenda"
date = 2019-12-18T09:55:39-05:00
weight = 110
+++

## Day 1
- Introduction
    - Dojo Requirements
    - Introduction
    - App? What App?
    - Sample Project Setup
- Build
    - mvn build dojo
- Deploy
    - Gitlab deployments
    - More Deployments

## Day 2
- Work
    - How does work get done
    - Git: A new way of working
- Test
    - Who Broke the build?
    - Test all the things!
    - That's not supposed to happen
- Conclusion
