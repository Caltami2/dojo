+++
title = "Background"
date = 2019-12-18T10:58:19-05:00
weight = 105
chapter = true
description = "Requirements for the labs"
+++

<div class="container">
  <h1> The Enterprise Delivery Platform
  </h1>
  <p class="lead"> You are likely to be familiar with the Enterprise Delivery Platform and its history; however, the following information is not in place to inform you about the history of EDP, but is to serve as a quick review of the driving factors behind the next evolution of T-Mobile development.    </p>
  <div class="card mt-4" >
    <div class="card-header">
      EDP's beginnings
    </div>
    <div class="p-4">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec suscipit nulla. Sed ac libero placerat, laoreet nibh sed, maximus erat. Aliquam erat volutpat. Cras vitae lectus at urna lacinia lobortis nec eget justo. Aenean mollis nisi ut est pretium, in ultrices nisi interdum. Curabitur facilisis nisl justo, vitae iaculis mauris auctor sit amet. Nulla facilisi. Fusce dictum, felis et fringilla viverra, nibh nulla sagittis mauris, nec vehicula lacus nisi dapibus lacus. In nec nunc dolor. Nam sodales leo vitae tincidunt porttitor. Morbi non tincidunt leo, nec rutrum urna.
    </div>
  </div>
  <div class="card mt-4">
    <div class="card-header">
      The Evolution of EDP
    </div>
    <div class="p-4">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec suscipit nulla. Sed ac libero placerat, laoreet nibh sed, maximus erat. Aliquam erat volutpat. Cras vitae lectus at urna lacinia lobortis nec eget justo. Aenean mollis nisi ut est pretium, in ultrices nisi interdum. Curabitur facilisis nisl justo, vitae iaculis mauris auctor sit amet. Nulla facilisi. Fusce dictum, felis et fringilla viverra, nibh nulla sagittis mauris, nec vehicula lacus nisi dapibus lacus. In nec nunc dolor. Nam sodales leo vitae tincidunt porttitor. Morbi non tincidunt leo, nec rutrum urna.
    </div>
  </div>
  <div class="card mt-4" >
    <div class="card-header">
      In retrospective
    </div>
    <div class="p-4">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec suscipit nulla. Sed ac libero placerat, laoreet nibh sed, maximus erat. Aliquam erat volutpat. Cras vitae lectus at urna lacinia lobortis nec eget justo. Aenean mollis nisi ut est pretium, in ultrices nisi interdum. Curabitur facilisis nisl justo, vitae iaculis mauris auctor sit amet. Nulla facilisi. Fusce dictum, felis et fringilla viverra, nibh nulla sagittis mauris, nec vehicula lacus nisi dapibus lacus. In nec nunc dolor. Nam sodales leo vitae tincidunt porttitor. Morbi non tincidunt leo, nec rutrum urna.
    </div>
  </div>
  <div class="card mt-4" >
    <div class="card-header">
      This is EDP Now
    </div>
    <div class="p-4">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec suscipit nulla. Sed ac libero placerat, laoreet nibh sed, maximus erat. Aliquam erat volutpat. Cras vitae lectus at urna lacinia lobortis nec eget justo. Aenean mollis nisi ut est pretium, in ultrices nisi interdum. Curabitur facilisis nisl justo, vitae iaculis mauris auctor sit amet. Nulla facilisi. Fusce dictum, felis et fringilla viverra, nibh nulla sagittis mauris, nec vehicula lacus nisi dapibus lacus. In nec nunc dolor. Nam sodales leo vitae tincidunt porttitor. Morbi non tincidunt leo, nec rutrum urna.
    </div>
  </div>
  <div class="card mt-4" >
    <div class="card-header">
      Lessons learned.
    </div>
    <div class="p-4">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec suscipit nulla. Sed ac libero placerat, laoreet nibh sed, maximus erat. Aliquam erat volutpat. Cras vitae lectus at urna lacinia lobortis nec eget justo. Aenean mollis nisi ut est pretium, in ultrices nisi interdum. Curabitur facilisis nisl justo, vitae iaculis mauris auctor sit amet. Nulla facilisi. Fusce dictum, felis et fringilla viverra, nibh nulla sagittis mauris, nec vehicula lacus nisi dapibus lacus. In nec nunc dolor. Nam sodales leo vitae tincidunt porttitor. Morbi non tincidunt leo, nec rutrum urna.
    </div>
  </div>
</div>
