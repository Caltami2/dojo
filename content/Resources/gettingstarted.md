+++
title = "How to get started"
date = 2019-12-18T09:55:39-05:00
weight = 400
chapter = true
pre = "<b> </b>"
+++

### Getting started on GitLab

<div class="card mt-4" >
  <div class="card-header">
    How to Log In
  </div>
  <div class="p-4">
  The following steps will allow you to login to GitLab. Please remember that this is a SaaS instance of gitlab.com and contains many other users and companies, please be sensitive to offering permissions and project visibility to just T-Mobile)
<br />
<br />
<ul class="list-group">
  <li class="list-group-item">
   0. While on VPN, Go <a href="https://cdp-ramp.us-west-2.elasticbeanstalk.com"> here </a> and login with your NTID and password to be automatically added to AD groups.
  </li>
  <li class="list-group-item">
1. Register with your user@t-mobile.com e-mail with Gitlab and use your NTID as your username. (If your account was already provisioned, go to reset password)
  </li>
  <li class="list-group-item">
2. Setup 2FA (Go to your Profile Settings.Go to Account.Click Enable Two-Factor Authentication.)
  </li>
  <li class="list-group-item">
3. Go here to link your gitlab account with your t-mobile SSO: <a href="https://gitlab.com/groups/tmobile/-/saml/sso">https://gitlab.com/groups/tmobile/-/saml/sso</a>
  </li>
  <li class="list-group-item">
4. Ask your Team Lead/Sub-Group owner to be promoted higher than guest for your sub-group under gitlab.com/tmobile
  </li>
  <li class="list-group-item">
5. If you want the beta gitlab.com experience - <a href="https://next.gitlab.com/">https://next.gitlab.com/</a> - please take caution a lot of features are not fully supported yet.
</li></ul></div></div>
<br />
<br />
<div class="card mt-4" >
  <div class="card-header">
How to create a new sub-group
  </div>
  <div class="p-4">
<ul class="list-group">
  <li class="list-group-item">
1. Shout out to @cdpsupport that you need a new subgroup created under gitlab.com/tmobile with your group name (in #cdp-support)
</li>
<li class="list-group-item">
2. Your group will be created and you will be made owner of that group.
</li>
  <li class="list-group-item">
3. Base sub-groups are maintained here: https://gitlab.com/tmobile/cdp/gitlab-base/tree/tmo/master/cdp-acls
</li></ul></div></div>
<br />
<br />
<div class="card mt-4" >
  <div class="card-header">
Tutorials
  </div>
  <div class="p-4">
<ul class="list-group">
  <li class="list-group-item">
Getting Started with Gitlab at T-Mobile: https://t-mo.co/360F1Uu
</li>
<li class="list-group-item">
Cornerstone Training: http://tm/cdptraining
</li>
<li class="list-group-item">
If you want to just learn some basics - check this training video out: https://t-mobile1.webex.com/webappng/sites/t-mobile1/recording/66c012f857474135b31225478caf4e96
</li>
<li class="list-group-item">
Variable Priority: https://docs.gitlab.com/ee/ci/variables/#priority-of-environment-variables
</li>
</ul>
</div>
</div>
<br />
<br />
<div class="card mt-4" >
  <div class="card-header">
Examples:
  </div>
  <div class="p-4">
<ul class="list-group">
  <li class="list-group-item">
If you want to view evolution of .gitlab-ci.yml in helloworld format go here: https://gitlab.com/tmobile/templates_projects
</li>
<li class="list-group-item">
Shared Templates: https://gitlab.com/tmobile/templates
</li>
<li class="list-group-item">
Services Side-car: https://gitlab.com/tmobile/cdp/cdp-service
</li>
<li class="list-group-item">
Shared Services: https://gitlab.com/tmobile/shared
</li></ul></div></div>
<br />
<br />

<div class="card mt-4" >
  <div class="card-header">
Ops:
  </div>
  <div class="p-4">
<ul class="list-group">
<li class="list-group-item">
If you would like to see live gitlab.com status: https://status.gitlab.com/
</li>
<li class="list-group-item">
If you want like to explore gitlab training: https://docs.gitlab.com/ee/university/
</li>
<li class="list-group-item">
Post Mortems: https://gitlab.com/tmobile/cdp/post-mortems
</li>
<li class="list-group-item">
Support: Use @cdpsupport callout in #cdp-support
</li>
<li class="list-group-item">
Gitlab.com version - https://gitlab.com/help
</li>
<li class="list-group-item">
Gitlab.com T-mobile Issue Tracker: https://gitlab.com/gitlab-com/account-management/pre-sales/t-mobile-enterprise-account-management/issues/36
</li>
<li class="list-group-item">
Gitlab @ T-Mobile Stats: https://bit.ly/2q1PuPt
</li>
<li class="list-group-item">
CDP Onboarding Stats: https://bit.ly/2NpmclQ
</li>
<li class="list-group-item">
Gitlab Direction: https://about.gitlab.com/direction/maturity/
     </li></ul></div></div>
<br />
<br />

<div class="card mt-4" >
  <div class="card-header">
404 when attempting to access gitlab.com/tmobile?
  </div>
  <div class="p-4">
<ul class="list-group">
</li>
<li class="list-group-item">
   Have you setup two-factor authentication(2FA)? https://gitlab.com/profile/account
   </li>
   <li class="list-group-item">
   Have you sso'ed? https://gitlab.com/groups/tmobile/-/saml/sso
   </li>
   <li class="list-group-item">
   Is there a blue error message when you browse to this page https://gitlab.com/profile?nav_source=navbar right after browsing to gitlab.com/tmobile?
   </li>
   <li class="list-group-item">
   Do you have multiple accounts which have SSO'ed? Login to older accounts, goto https://gitlab.com/profile/account, and click Disconnect on SAML for tmobile, then re-SSO with new account.
</li>
</ul></div></div>
<br />
<br />
<div class="card mt-4" >
  <div class="card-header">
Other problems?
  </div>
  <div class="p-4">

Need to recover 2FA or have other issues not listed?
<br/>
If you need to file a support ticket with gitlab: Register will your T-Mobile e-mail @ https://support.gitlab.com/hc/en-us/requests/new and file a support ticket with gitlab directly.
</div>
</div>
<br />
<br />
