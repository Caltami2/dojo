DOCKER       = docker
HUGO_VERSION = 0.60.1
DOCKER_IMAGE = dojo-hugo
DOCKER_RUN   = $(DOCKER) run --rm --interactive --tty --volume $(PWD):/src

.PHONY: all build build-preview serve docker-all

all: build serve ## Build site with production settings

clean:
	rm -rf docs/

build: ## Build site with production settings
	hugo

build-preview: ## Build site with drafts and future posts enabled
	hugo -D -F

production-build: build

non-production-build: ## Build the non-production site, which adds noindex headers to prevent indexing
	hugo --enableGitInfo

serve: ## Boot the development server.
	hugo server --ignoreCache --disableFastRender

docker-all: docker-image docker-serve

docker-image:
	$(DOCKER) build . --tag $(DOCKER_IMAGE) --build-arg HUGO_VERSION=$(HUGO_VERSION)

docker-build:
	$(DOCKER_RUN) $(DOCKER_IMAGE) hugo

docker-serve:
	$(DOCKER_RUN) -p 1313:1313 $(DOCKER_IMAGE) hugo server --watch --bind 0.0.0.0
